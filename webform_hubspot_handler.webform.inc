<?php

/**
 * @file
 * Integrates third party settings on the module's behalf.
 */

use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_FORM_ID_alter().
 */
function webform_hubspot_handler_webform_admin_third_party_settings_form_alter(&$form, FormStateInterface $form_state) {
  /** @var Drupal\webform\WebformThirdPartySettingsManagerInterface $third_party_settings_manager */
  $third_party_settings_manager = \Drupal::service('webform.third_party_settings_manager');
  $api_key = $third_party_settings_manager->getThirdPartySetting('webform_hubspot_handler', 'api_key');
  $portal_id = $third_party_settings_manager->getThirdPartySetting('webform_hubspot_handler', 'portal_id');

  // Admin settings.
  $form['third_party_settings']['webform_hubspot_handler'] = [
    '#type' => 'details',
    '#title' => t('Webform Hubspot Handler'),
    '#open' => TRUE,
    '#description' => t('Global API settings for the Hubspot webform handler.'),
  ];

  $form['third_party_settings']['webform_hubspot_handler']['api_key'] = [
    '#type' => 'textfield',
    '#title' => t('Hubspot API Key'),
    '#default_value' => $api_key,
  ];
  $form['third_party_settings']['webform_hubspot_handler']['portal_id'] = [
    '#type' => 'textfield',
    '#title' => t('Hubspot Portal ID'),
    '#default_value' => $portal_id,
  ];

}
