<?php

namespace Drupal\webform_hubspot_handler\Plugin\WebformHandler;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\TitleResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Link;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Serialization\Yaml;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\Utility\WebformOptionsHelper;
use Drupal\webform\WebformSubmissionConditionsValidatorInterface;
use Drupal\webform\WebformSubmissionInterface;
use Drupal\webform\WebformThirdPartySettingsManagerInterface;
use Drupal\webform\WebformTokenManagerInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Cmf\Component\Routing\RouteObjectInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Webform submission Hubspot handler.
 *
 * @WebformHandler(
 *   id = "hubspot",
 *   label = @Translation("Hubspot"),
 *   category = @Translation("External"),
 *   description = @Translation("Posts webform submissions to a mapped Hubspot form."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_SINGLE,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED
 * )
 */
class HubspotWebformHandler extends WebformHandlerBase {

  /**
   * The API key for the Hubspot account.
   *
   * @var string
   */
  private $apiKey;

  /**
   * The portal identifier of the Hubspot account.
   *
   * @var int
   */
  private $portalID;

  /**
   * The HTTP client to fetch the feed data with.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The token manager.
   *
   * @var \Drupal\webform\WebformTranslationManagerInterface
   */
  protected $tokenManager;
  /**
   * The current request.
   *
   * @var \Symfony\Component\HttpFoundation\Request
   */
  private $request;
  /**
   * Resolves a title based on a route and request.
   *
   * @var \Drupal\Core\Controller\TitleResolverInterface
   */
  private $titleResolver;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LoggerChannelFactoryInterface $logger, ClientInterface $http_client, WebformTokenManagerInterface $token_manager, Request $request, TitleResolverInterface $title_resolver, WebformThirdPartySettingsManagerInterface $third_party_manager, EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, WebformSubmissionConditionsValidatorInterface $conditions_validator) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger, $config_factory, $entity_type_manager, $conditions_validator);

    $this->httpClient = $http_client;
    $this->tokenManager = $token_manager;
    $this->request = $request;
    $this->titleResolver = $title_resolver;

    $this->apiKey = $third_party_manager->getThirdPartySetting('webform_hubspot_handler', 'api_key');
    $this->portalID = (int) $third_party_manager->getThirdPartySetting('webform_hubspot_handler', 'portal_id');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.factory'),
      $container->get('http_client'),
      $container->get('webform.token_manager'),
      $container->get('request_stack')->getCurrentRequest(),
      $container->get('title_resolver'),
      $container->get('webform.third_party_settings_manager'),
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('webform_submission.conditions_validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getSummary() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'hubspot_form' => '',
      'field_mapping' => '',
      'custom_data' => '',
      'values_as_keys' => FALSE,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $this->getWebform();

    // Gets an array of select options containing all available Hubspot forms.
    $hubspot_forms = $this->getHubspotForms();

    // Determine the currently selected Hubspot form.
    $selected_form = key($hubspot_forms);
    if (isset($form_state->getUserInput()['settings'], $form_state->getUserInput()['settings']['hubspot_form'])) {
      $selected_form = $form_state->getUserInput()['settings']['hubspot_form'];
    }
    elseif ($this->configuration['hubspot_form']) {
      $selected_form = $this->configuration['hubspot_form'];
    }

    // Display a warning if the API key or Portal ID is not found.
    $this->checkHubspotApiSettings($form);

    // Start building the actual form.
    // First the user needs to select a Hubspot form.
    $form['hubspot_form'] = [
      '#type' => 'select',
      '#options' => $hubspot_forms,
      '#title' => $this->t('Hubspot form'),
      '#description' => $this->t('Select the Hubspot form you wish to post to.'),
      '#required' => TRUE,
      '#default_value' => $selected_form,
      '#ajax' => [
        'callback' => [$this, 'fieldMappingAjaxCallback'],
        'wrapper' => 'hubspot-field-mapping-wrapper',
      ],
    ];

    // Disable the select if no forms could be loaded.
    if (empty($hubspot_forms)) {
      $form['hubspot_form']['#disabled'] = TRUE;
      $form['hubspot_form']['#options'] = [
        0 => $this->t('No Hubspot forms found.'),
      ];
    }

    // After selecting a form, the user needs to map the Webform fields to the
    // Hubspot fields. Build a table with two columns.
    $form['field_mapping'] = [
      '#type' => 'table',
      '#title' => $this->t('Field mapping'),
      '#description' => $this->t('Map the fields of the webform to the correct fields of the Hubspot form'),
      '#prefix' => '<div id="hubspot-field-mapping-wrapper">',
      '#suffix' => '</div>',
      '#header' => [
        $this->t('Webform field'),
        $this->t('Hubspot field'),
      ],
    ];

    $form['values_as_keys'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Pass selectbox keys instead of values'),
      '#default_value' => $this->configuration['values_as_keys'],
      '#description' => $this->t('When using select elements, by default the internal value is passed. Checking this box switches this behavior so that the label is passed instead.')
    ];

    // Fields can only be mapped if a Hubspot form was selected.
    if ($selected_form) {
      // This is a list of select options, consisting of all the Hubspot fields.
      $hubspot_fields = $this->getHubspotFormFields($selected_form);

      // Contains an array of all the elements on this webform.
      $webform_fields = $webform->getElementsDecodedAndFlattened();

      // Create a row in the table for each webform field. Create select in the
      // second column to map the correct Hubspot form field.
      foreach ($webform_fields as $machine_name => $field) {
        $saved_value = FALSE;
        if (!empty($this->configuration['field_mapping'])) {
          $saved_value = $this->configuration['field_mapping'][$machine_name]['hubspot_field'];
        }

        $default_value = key($hubspot_fields);
        if (!empty($saved_value) && $hubspot_fields[$saved_value]) {
          $default_value = $saved_value;
        }

        $form['field_mapping'][$machine_name] = [
          'webform' => [
            '#markup' => $field['#title'] ?? $machine_name,
          ],
          'hubspot_field' => [
            '#type' => 'select',
            '#options' => $hubspot_fields,
            '#default_value' => $default_value,
          ],
        ];
      }
    }

    // Allow to send custom data to the Hubspot form.
    $form['custom_data'] = [
      '#type' => 'details',
      '#title' => $this->t('Custom data'),
      '#description' => $this->t('Custom data will take precedence over submission data. You may use tokens.'),
    ];

    $form['custom_data']['custom_data'] = [
      '#type' => 'webform_codemirror',
      '#mode' => 'yaml',
      '#title' => $this->t('Custom data'),
      '#description' => $this->t('Enter custom data that will be included in all remote posts. This can be used to post fixed values to certain Hubspot fields.'),
      '#parents' => ['settings', 'custom_data'],
      '#default_value' => $this->configuration['custom_data'],
    ];

    $form['custom_data']['token_tree_link'] = $this->tokenManager->buildTreeLink();

    return $form;
  }

  /**
   * Attempts to load forms via the Hubspot API to put them in a select list.
   *
   * @return array
   *   An array of select options containing the available Hubspot forms.
   */
  public function getHubspotForms(): array {
    try {
      /** @var \GuzzleHttp\Psr7\Response $response */
      $response = $this->httpClient->get('https://api.hubapi.com/forms/v2/forms?hapikey=' . $this->apiKey);
    }
    catch (ClientException $e) {
      $this->handleException($e);

      return [];
    }

    /** @var array $data */
    $data = json_decode($response->getBody()->getContents());

    $forms = [];
    foreach ($data as $form) {
      if ($form->portalId !== $this->portalID) {
        continue;
      }

      $forms[$form->guid] = $form->name;
    }

    if (count($forms) > 0) {
      array_unshift($forms, $this->t('- Select a form -'));
    }

    return $forms;
  }

  /**
   * Tries to load all the defined fields of a Hubspot form via the Hubspot API.
   *
   * @param string $form_id
   *   The unique identifier of the Hubspot form.
   *
   * @return array
   *   Returns an array of select options containing the available form fields.
   */
  public function getHubspotFormFields($form_id): array {
    try {
      /** @var \GuzzleHttp\Psr7\Response $response */
      $response = $this->httpClient->get('https://api.hubapi.com/forms/v2/fields/' . $form_id . '?hapikey=' . $this->apiKey);
    }
    catch (ClientException $e) {
      $this->handleException($e);

      return [];
    }

    /** @var array $data */
    $data = json_decode($response->getBody()->getContents());

    $fields = [0 => $this->t('- Select a field -')];
    foreach ($data as $hubspot_field) {
      $fields[$hubspot_field->name] = $hubspot_field->label;
    }

    return $fields;
  }

  /**
   * Refreshes the field_mapping form element when the user selects a form.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form_state.
   *
   * @return array
   *   Returns the specific form element which needs to be refreshed.
   */
  public function fieldMappingAjaxCallback(array &$form, FormStateInterface $form_state): array {
    return $form['settings']['field_mapping'];
  }

  /**
   * Adds a warning to the form is the Hubspot API Key or Portal ID are missing.
   *
   * @param array $form
   *   The configuration form of the handler.
   */
  protected function checkHubspotApiSettings(array &$form) {
    if (!$this->apiKey || !$this->portalID) {
      $link = Link::createFromRoute($this->t('third party settings'), 'webform.config')->toString();

      $form['warning'] = [
        '#type' => 'webform_message',
        '#message_type' => 'warning',
        '#message_message' => $this->t("Don't forget to set the API Key and Portal ID in the @link.", ['@link' => $link]),
      ];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);

    $values = $form_state->getValues();
    foreach ($this->configuration as $name => $value) {
      if (isset($values[$name])) {
        $this->configuration[$name] = $values[$name];
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function postSave(WebformSubmissionInterface $webform_submission, $update = TRUE) {
    // Only post a form to Hubspot when it is completed.
    if ($webform_submission->isCompleted()) {
      $this->remotePost($webform_submission);
    }
  }

  /**
   * Execute a remote post.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission to be posted.
   */
  protected function remotePost(WebformSubmissionInterface $webform_submission) {
    // The URL to post to.
    $request_url = $this->buildRequestUrl();

    // The formatted data in x-www-form-urlencoded.
    $request_post_data = $this->getPostData($webform_submission);

    try {
      $this->httpClient->post($request_url, ['form_params' => $request_post_data]);
    }
    catch (ClientException $e) {
      $this->handleException($e);
    }

  }

  /**
   * Get a webform submission's post data.
   *
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission to be posted.
   *
   * @return array
   *   A webform submission converted to an associative array.
   */
  protected function getPostData(WebformSubmissionInterface $webform_submission): array {
    // Get submission and elements data.
    $data = $webform_submission->toArray(TRUE);

    if ($this->configuration['values_as_keys']) {
      $this->replaceValuesWithKeys($data, $webform_submission);
    }

    // Define the array we'll return.
    $values = [];

    // Flatten data.
    // Prioritizing elements before the submissions fields.
    $data = $data['data'] + $data;
    unset($data['data']);

    // Try to get the current page title.
    $title = '';
    $route = $this->request->attributes->get(RouteObjectInterface::ROUTE_OBJECT);
    if ($route) {
      $title = $this->titleResolver->getTitle($this->request, $route);
    }

    // Build the hs_context object for Hubspot.
    $hs_context = [
      'hutk' => $this->request->cookies->get('hubspotutk'),
      'ipAddress' => $data['remote_addr'],
      'pageUrl' => $data['uri'],
      'pageName' => $title,
    ];
    $values['hs_context'] = json_encode($hs_context);

    // Append custom data and replace tokens.
    if (!empty($this->configuration['custom_data'])) {
      $custom_yaml_data = $this->configuration['custom_data'];
      // Replace tokens.
      $custom_yaml_data = $this->tokenManager->replace($custom_yaml_data, $webform_submission);
      // Add the custom data to the values array for posting.
      $values += Yaml::decode($custom_yaml_data);
    }

    // Load the configured field mapping.
    $field_mapping = $this->configuration['field_mapping'];

    // Map the submitted field values to the Hubspot fields.
    /** @var array $field_mapping */
    foreach ($field_mapping as $webform_field => $map) {
      if (!(bool) $map['hubspot_field']) {
        continue;
      }

      $values[$map['hubspot_field']] = $data[$webform_field];
    }

    return $values;
  }

  /**
   * Builds the Hubspot URL where we need to post the form to.
   *
   * @return string
   *   Returns the request url to POST the form submission to.
   */
  protected function buildRequestUrl(): string {
    $request_url = 'https://forms.hubspot.com/uploads/form/v2/:portal_id/:form_guid';

    $search = [':form_guid', ':portal_id'];
    $replace = [$this->configuration['hubspot_form'], $this->portalID];
    $request_url = str_replace($search, $replace, $request_url);

    return (string) $request_url;
  }

  /**
   * Logs on error message in the Drupal error log.
   *
   * @param mixed $exception
   *   Contains the thrown exception from Guzzle.
   */
  protected function handleException($exception) {
    $message = $exception->getMessage();

    // Encode HTML entities to prevent broken markup from breaking the page.
    $message = nl2br(htmlentities($message));

    // Log error message.
    $context = [
      '@form' => $this->getWebform()->label(),
      '@message' => $message,
      'link' => $this->getWebform()
                     ->toLink($this->t('Edit'), 'collection')
                     ->toString(),
    ];
    $this->getLogger()->error('@form webform post to Hubspot failed. @message', $context);
  }

  /**
   * For elements with select options, replace the element's value with the key.
   *
   * @param array $data
   *   The submitted data of the webform submission.
   * @param \Drupal\webform\WebformSubmissionInterface $webform_submission
   *   The webform submission to be posted.
   */
  protected function replaceValuesWithKeys(array &$data, WebformSubmissionInterface $webform_submission) {
    /** @var \Drupal\webform\WebformInterface $webform */
    $webform = $webform_submission->getWebform();

    foreach ($data['data'] as $field => $value) {
      $element = $webform->getElementDecoded($field);

      if (!array_key_exists('#options', $element)) {
        continue;
      }

      $key = WebformOptionsHelper::getOptionText($value, $element['#options']);

      $data['data'][$field] = $key;
    }
  }

}
