# ABOUT
This module provides a Webform handler which sends form data on submission to a linked Hubspot form.
On every form submit, a new submission is created in Hubspot as well.

When configuring the handler, you choose a Hubspot form you wish to post to.
Then you map the fields between the Webform form and Hubspot form.

You can also pass custom data. E.g. when the Hubspot form has a field that is not present in your webform.

The module is compatible with Webform 8.x-5.0

# CONFIGURATION
- Enable the module as usual.
- Go to the global third party settings of the Webform module and fill in your credentials.
-- /admin/structure/webform/config
- Edit a webform of your choice, and add the Hubspot handler.
- Select a form in the dialog and map the fields between the two forms.


# AUTHOR
written by Screon.
First published on 20/07/2017.
